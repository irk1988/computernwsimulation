/* 
 * Program to implement the shortest path for a given undirected graph G = (V,E)
 * Author : Philip Karunakaran, Immanuel Rajkumar
 */

#include<iostream>
#include<ctime>
#include<vector>
#include<list>
#include<queue>
#include<climits>
#include<utility>
#include<fstream>

using namespace std;

int INF;

class Vertex;

/*
 * Edge -  Represents an edge in the graph which are connected by two 'Vertex's
 */
class Edge {

	public:

	Vertex * from;
	Vertex * to;
	int weight;
	int r;

	Edge ( Vertex * u, Vertex * v, int w, int r ) {
		this->from = u;
		this->to = v;
		this->r = r;
		this->weight = w;
	}

	Vertex * otherEnd(Vertex * u ) {
		if( from == u ) return to;
		else return from;
	}

	bool operator < ( const Edge * e ) const {
		return this->weight < e->weight;
	}
};

/*
 * Vertex - Represents a vertex in the graph 
 */
class Vertex {

	public:

	int name;
	bool seen;
	Vertex * parent;
	int distance;
	int r;
	list<Edge *> adj;
	list<Vertex *> path;

	Vertex (int n) {
		this->name = n;
		this->seen = false;
		this->parent = NULL;
		this->distance = 0;
		this->r=0;
	}
};

/* Vertex comparator */
struct comparator {
	bool operator () ( const Vertex * u, const Vertex * v) {
		return u->distance > v->distance;
	}
};

/*
 * Graph representation of G=(V,E)
 */
class Graph {
	
	public:

	vector<Vertex *> V;
	int n;

	Graph (int s) {
		this->n = s;
		V.resize(s+1);
		for( int i=0; i<=s; i++ ) 
			V[i] = new Vertex(i);
	}

	void addEdge( int u, int v, int w, int r) {
		Edge * e  = new Edge( V[u], V[v], w, r );
		V[u]->adj.push_back(e);
		V[v]->adj.push_back(e);
	}

	int BFS(Vertex * u) {
		queue<Vertex *> q;
		q.push(u);
		u->seen = true;	
		Vertex * x;
		while( !q.empty() ) {
			x = q.front();
			q.pop();

			list<Edge *>::iterator i, BEGIN = x->adj.begin(), END = x->adj.end();
			for( i=BEGIN; i!=END; ++i ) {
				Vertex * v = (*i)->otherEnd(x);
				if( !v->seen ) {
					v->distance = x->distance + 1;	
					v->seen = true;
					v->parent = x;
					v->r = (*i)->r;
					q.push(v);
				}
			}	
		}
		return 0;
	}

	int ComputePaths( ) {
		vector<Vertex *>::iterator i, BEGIN = this->V.begin(), END = this->V.end();
		for( i = ++BEGIN; i != END; ++i ) {
			Vertex * curVertex = (*i);
			Vertex * prevHop = (*i)->parent;
			(*i)->path.push_back(curVertex);
			while( prevHop != NULL ) {
				(*i)->path.push_back(prevHop);
				prevHop = prevHop->parent;
			}
		}
		return 0;
	}

	int DisplayPaths( int s, int time, string fname ) {
		list< pair < pair<int,int>, int > > result;
		vector<Vertex *>::iterator i, BEGIN = this->V.begin(), END = this->V.end();
		for( i = ++BEGIN; i != END; ++i ) {
			list<Vertex *>::reverse_iterator j, JBEGIN = (*i)->path.rbegin(), JEND = (*i)->path.rend();
			int hop=0;
			for( j = JBEGIN; j != JEND; ++j ) {
				hop++;
				if( hop == 2 ) {
					result.push_back( make_pair( make_pair(s, (*i)->name), (*j)->r ));
				}
			}
		}

		ofstream outfile( fname.c_str() , std::ios::app);
		outfile<<"\n ROUTING TABLE STATE : (At Time : "<<(90 - time)<<")";
		list< pair< pair<int,int>, int> >::iterator k, KB = result.begin(), KE = result.end();
		for( k=KB; k!=KE; ++k ) {
			outfile<<"\n LAN "<<k->first.first<<" -->  LAN "<<k->first.second<<"  NEXT HOP ROUTER : "<<k->second <<"  TIME : "<<(90 - time);
		}
		outfile.close();

		return 0;
	}

	int PrintVertexTable( ) {
		vector<Vertex *>::iterator i, BEGIN = this->V.begin(), END = this->V.end();
		cout<<"\nPrinting the SPT\n";
		for( i = ++BEGIN; i != END; ++i ) {
			cout<<"\n Vertex : "<<(*i)->name<<"  Distance from src : "<<(*i)->distance;
			if( (*i)->parent != NULL ) cout<<"  Parent : "<<(*i)->parent->name;
		}
		cout<<endl;
		return 0;
	}

};


//list< pair< pair<int,int> > > driver( Graph& g, int s ) {
int driver( Graph& g, int s, int time, string fname) {
	Vertex * src = g.V[s];
	g.BFS(src);
	g.ComputePaths();
	//g.PrintVertexTable();
	g.DisplayPaths(s, time, fname);
	return 0;
}

/*
int main() {

	INF = INT_MAX;
	clock_t start, end;
	double dur = 0;
	start = clock();

	int en, vn;
	int src, t;

	cin>>vn>>en>>src;
	
	Graph g(vn);

	int u, v, w;	
	for( int i=0; i<en; i++ ) {
		cin>>u>>v>>w;
		g.addEdge(u,v,w);
	}
	
	driver(g);
	
	end = clock();

	dur = (double)(end - start)/CLOCKS_PER_SEC*1000;
	cout<<"\nRuntime: "<<dur<<" msec"<<endl;

	return 0;
}
*/
