/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Implementing OSPF and BGP
 * Sub module : brouter
 */

#include<iostream>
#include<sstream>
#include<fstream>
#include<unistd.h> /* For sleep */
#include<list>
#include<map>
#include<set>
#include<algorithm>

#include "Graph.h"

using namespace std;

/*
 * Each router is associated with a netID and hostID. This structure defines a container for holding the identification details
 */
struct ID {
    public:
	int netID; /* NN -network ID - The network ID to which this router belongs to */
	int hostID; /* HH - Unique host name/ID for this router in a network */

	/* Constructors - 1) Parameterized 2) Default */
	ID (int n, int h ) {
		netID = n;
		hostID = h;
	}

	ID() {
		netID=0;
		hostID=0;
	}
};

/*
 * Global AS Mapping, this is populated in the main function
 * Brouter reads this map and adds only those related to it in the brouter class
 */
map<int,int> ProviderMap;
map<int,int> PeerMap;


/*
 * Data Structure to store the properties of a peer - both interior and exerior peer
 */
struct Peer {
    public:
	int ASNo;
	ID ipAddress; //The IP address <nn,hh> of the peer
	int uid;
};
	

/*
 * Data Structure to store the topology table for this router.
 * Contains mapping of router --> Lans mapping
 */
struct TopologyTable {
    public:
	map<int, list<int> > table;
};


/* Utility function to convert string to int */
int toInt( string s ){
	int result = 0;
	stringstream ss(s);
	ss >> result;
	ss.clear();
	return result;
}
	
/*
 * Router Type - brouter
 * Each router is identfied by (NN,HH) and AS#
 */
class BRouter {

    public:
	int AS; /*Each brouter is associated witb a single AS*/
	list<ID> names; /* Each router is associated with many network. This list contains the (NN,HH) address information of the router*/
	int uid;  /* Unique ID for a router in the whole network */
	int nwcount; /* Number of networks the router is connected to. Programatically, size of name list */
	int bcastID;

	/* Properties specific to a brouter */
	list<Peer> iPeers; /* Interior peer list */
	list<Peer> ePeers; /* Exterior peer list */
	TopologyTable topology;

	list<int> injNWs; /* List of networks that this router will inject in its LSA */
	
	/*Provider-Peer list for this class*/
	list<int> PeerAS;
	list<int> ProviderAS;
	
	string outFileName;
	string routingFileName;

	/* Utility variable for string and int manipulation */
	map<int, long int> lastRead; //Map to maintain the lastreadbyte of a netYY file. Mapping YY -> lastReadByte
	map<int, map<int, long int> > lastBCastIDforNW; // Map to maintain lastBcastIDforNW[uid][nwNo]	

	stringstream s;
	
	/* Constructor */
	BRouter(int uniqueID, int asNum, int netCount) {
		uid = uniqueID;
		AS = asNum;
		nwcount = netCount;
		bcastID = 1;

		stringstream ss("");
		s << "OUT";
		if( this->uid < 10 ) s << "0";
		s << this->uid;
		this->outFileName = s.str();
		
		//Setting the routing file name in constructor
		stringstream yy;
		yy << "RT";
		if( this->uid < 10 ) yy << "0";
		yy << this->uid;
		this->routingFileName = yy.str();
		yy.clear();
    	}

	/*
	 * Populate the list of Peer AS and Provider AS
	 */
	int PopulatePeerProviderList() {
		map<int, int>::iterator it, BEGIN=ProviderMap.begin() , END=ProviderMap.end();
		for( it=BEGIN; it!=END; ++it ) {
			if( it->second == this->AS ) 
				ProviderAS.push_back(it->first);
		}

		BEGIN=PeerMap.begin(), END=PeerMap.end();
		for( it=BEGIN; it!=END; ++it ) {
			if( it->first == this->AS )
				PeerAS.push_back(it->second);
			else if( it->second == this->AS )
				PeerAS.push_back(it->first);

		}
		/*
		list<int>::iterator itr, LBEGIN=ProviderAS.begin(), LEND=ProviderAS.end();
		for( itr=LBEGIN; itr!=LEND; ++itr )
			cout<<"\nProvider :"<<(*itr);
		
		cout<<endl;
		LBEGIN=PeerAS.begin(), LEND=PeerAS.end();
		for( itr=LBEGIN; itr!=LEND; ++itr )
			cout<<"\nPeer :"<<(*itr);
		cout<<endl;
		*/
		return 0;
	}

	/*
	 * This routine calls the various functions of the router, at the appropriate time interval 
	 * Ex. LSA advertisements need to be done every 10 secs, this routine calls LSA advertisement routine every 10 secs 
         */
	int OperationsController( int lifetime ) {
		initLastReadState();
		initAliveMsg();
		int i = lifetime;

		while( --i > 0 ) {
			if( i % 10 == 0 ) //Need to change later
				LSAaddvertise(i);
			
			/* Reading Net files associated with the router every second */
			ReadNetFiles(i);

			/* Compute the routing table every 5 sec (Change the time later)*/ 
			if( i % 15 == 0 )
				ComputerRoutingTable(i);
				
			sleep(1);
		}
		return 0;
	}
	
	/* This routine computes the Routing table as per the schedule - Uses a graph class from external file */
	int ComputerRoutingTable( int tick ) {
		//cout<<"\n Computing the routing table at : "<<tick;
		int src = this->uid;

		set<int> LANS; //using a set data structure to store the unique LANS // NOTE: In set there are no duplicates
		map<int, list<int> >::iterator mit, MBEGIN=this->topology.table.begin(), MEND=this->topology.table.end();
		for( mit=MBEGIN; mit!=MEND; ++mit ) {
			list<int>::iterator cns, CNBEGIN=mit->second.begin(), CNEND=mit->second.end();
			for( cns=CNBEGIN; cns!=CNEND; ++cns ) {
				LANS.insert(*cns);
			}	
		}
	
		int vn = LANS.size();
	
		Graph g(vn);

		for( mit=MBEGIN; mit!=MEND; ++mit ) {
			list<int>::iterator ii, CNBEGIN=mit->second.begin(), CNEND=mit->second.end();
			for( ii=CNBEGIN; ii!=CNEND; ++ii ) {
				list<int>::iterator jj;
				for( jj = ii; jj!=CNEND; ++jj ) {
					if( ii != jj )
					g.addEdge((*ii),(*jj),1,mit->first);
				}
			}	
		}
		
		driver(g, src, tick, routingFileName);	
		
		return 0;
	};


	/* Put an entry into 'Routers' table stating it is alive now. This is used by controller so that it can read OUTXX files */
	int initAliveMsg() {
		ofstream outfile( "Routers", std::ios::app);
		stringstream ss;
		if ( this->uid < 10 ) ss << "0";
		ss << this->uid;
		outfile<<ss.str()<<endl;
		outfile.close();
		
		return 0;
	}

	/* Setting the last read state for resuming at this point in the next read from file */
	int initLastReadState() {
		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) 
			lastRead[i->netID] = 0;

		return 0;
	}

	/* Routine to create an LSA message and post it in the OUTXX file of the irouter */
	int LSAaddvertise( int tick ) {
		//cout<<"\nCalling the LSA advertisement routine at : "<<tick;
		ofstream outfile( outFileName.c_str(), std::ios::app);

		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) {
			outfile<<"(";
			if( i->netID < 10 ) outfile<<"0";
			outfile<<i->netID<<",";
			if( i->hostID < 10 ) outfile<<"0";
			outfile<<i->hostID<<") (";
			if( i->netID < 10 ) outfile<<"0";
			outfile<<i->netID<<",99) LSA ";
			if( this->uid < 10 ) outfile<<"0";
			outfile<<this->uid<<" "<<bcastID<<" NETWKS ";
			for( k=BEGIN; k!=END; ++k ) {
				if( k->netID < 10 ) outfile<<"0";
				outfile<<k->netID<<" ";
			}

			/* Inserting all the injected networks */
			list<int>::iterator j , JBEGIN = this->injNWs.begin() , JEND = this->injNWs.end();
			for( j=JBEGIN; j!=JEND; ++j ) {
				if( (*j) < 10 ) outfile<<"0";
				outfile<<(*j)<<" ";
			}

			outfile<<"OPTIONS AS "<<this->AS;

			if( injNWs.size() > 0 ) { 
				outfile<<" INJECTED ";
				for( j=JBEGIN; j!=JEND; ++j ) {
					if( (*j) < 10 ) outfile<<"0";
					outfile<<(*j)<<" ";
				}
			}

			outfile<<endl;//flushing output to file
		}
		this->bcastID++;
		outfile.close();
		s.clear();
		return 0;
	}

	/* Routine to read the NETYY files and update the routing table of the irouter */
	int ReadNetFiles( int tick ) {
		//cout<<"\nReading Net files every second : "<<tick;
		
		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) {
			s.str("");
			s << "NET";
			if ( i->netID < 10 ) s <<"0";
			s << i->netID;
			string filename = s.str();
			
			ifstream inputfile( filename.c_str());
			if( inputfile.is_open() ) {
				inputfile.seekg(lastRead[i->netID], inputfile.beg);
				string line;
				while(std::getline(inputfile,line)) {
					HandleNetFilesLine( line );
					lastRead[i->netID] = inputfile.tellg();
				}
			}
			inputfile.close();
			s.clear();
		}

		return 0;
	}

	/* Handles the net files line read. Appends the line to its own OUTXX file if its not its own broadcase */
	int HandleNetFilesLine( string msg ) {
	
		string part;
		s.str(msg);
		
		int partIndex = 1;
		int from = 0;
		ID senderIP(0,0);
		bool msgFromMe=true, newMsg=false;
		bool hasOptions=false, hasBorder=false, hasInjected=false;
		bool nwStart = false, injnwStart = false;
		int asIndex = -1;
		string asStr; int msgASNo = 0;
		list<int> nws; list<int> injnws;
		
		string bcastNW; int bcastNWNo;

		while( std::getline(s, part, ' ') ) {
			if( partIndex == 1 ) {
				senderIP.netID = toInt(part.substr(1,2));
				senderIP.hostID = toInt(part.substr(4,2));
			}
			
			if( partIndex == 2 ) {
				//Getting the destination bcast address
				bcastNW = part.substr(1,2);
				bcastNWNo = toInt(bcastNW);
			}
			
			if( partIndex == 4 ) {
				//Get the router ID from this part, check its bcast id
				from = toInt(part);
				//If this msg is not from me, then handle it.
				if( from != this->uid ) {
					msgFromMe = false;
				}
			}
		
			if( partIndex == 5 ) {
				//check the broadcast id if this is a msg from a different router
				if( !msgFromMe ) {
					int curBcastID;
					curBcastID = toInt(part);
					//Are we reading a msg from this reouter for the first time, check if there is n entry in the map
					if( lastBCastIDforNW[from][bcastNWNo] < curBcastID ) {
						//We have seen this router before, now checking if this is a new bcast
						lastBCastIDforNW[from][bcastNWNo] = curBcastID;
						newMsg = true;
					}
				}
			}

			if( part == "NETWKS" ) {
				nwStart = true;
			}
		
			if( part == "OPTIONS" ) {
				hasOptions=true;
				asIndex = partIndex + 1;
				nwStart = false;
			}
			
			if( nwStart ) {
				int conNW = 0;
				conNW = toInt(part);
				nws.push_back(conNW);
			}
	

			if( part == "BORDER" ) {
				hasBorder=true;
				asIndex = partIndex + 1;
			}

			if( part == "INJECTED" ) {
				hasInjected = true;
				injnwStart = true;
			}

			if( injnwStart ) {
				int conNW = 0;
				conNW = toInt(part);
				injnws.push_back(conNW);
			}

			if( partIndex == asIndex ) {
				//Getting the asNO;
				asStr = part;
			}	
		
			partIndex++;
		}
	
		//Deleting the empty first entry in both the injected and the non-injected networks
		if( nws.size() > 1 ) 
			nws.erase(nws.begin());
		if( injnws.size() > 1 ) 
			injnws.erase(injnws.begin());

		if( asIndex != -1 ) {
			msgASNo = toInt(asStr);
		}
		else if( asIndex == -1 ) {
			msgASNo = this->AS;
		}

		if( !hasOptions && !hasBorder && !hasInjected && newMsg ) {
			string modmsg = msg;
			stringstream oo;
			if( this->AS < 10 ) oo << "0";
			oo << this->AS;
			msg = msg + " OPTIONS " + oo.str();
			//Need to fwd this msg now
			ForwardMsgToOut(msg, bcastNW, bcastNWNo);
			this->topology.table[from] = nws;	
		}	

		if( hasOptions && !hasBorder && !hasInjected && newMsg ) {
			if( msgASNo == this->AS ) {
				cout<<"\nNew msg from irouter but with OPTIONS already added - Simply fwd it";
				//Need to fwd this msg now
				ForwardMsgToOut(msg, bcastNW, bcastNWNo);
				cout<<"\nModifying the topology table entry for : "<<from;
				this->topology.table[from] = nws;	
			}
			else {
				//cout<<"\nNew msg from an irouter outside AS - IGNORE";
			}
		}

		if( hasOptions && hasBorder && newMsg && (msgASNo == this->AS) ) {
			//Interior BGP Peer - Add it if not present
			list<Peer>::iterator it, IBEGIN=iPeers.begin(), IEND=iPeers.end();
			bool found=false;
			for( it=IBEGIN; it!=IEND; ++it ) 
				if( it->ASNo==msgASNo && it->uid==from ) {
					found=true; break;
				}
			if( !found ) {
				Peer p;
				p.uid = from;
				p.ASNo = msgASNo;
				p.ipAddress = senderIP;
				iPeers.push_back(p);
			}

			if( hasInjected ) {
				//Add non-injected networks to the topology table
				list<int>::iterator inw, INWBEGIN=injnws.begin(), INWEND=injnws.end();
				for( inw=INWBEGIN; inw!=INWEND; ++inw ) {
					nws.erase(std::find(nws.begin(), nws.end(), *inw));
				}
				this->topology.table[from] = nws;	
			} 
			else if( !hasInjected ) {
				//Add networks to the topology table
				this->topology.table[from] = nws;
			}
			//Forward the msg as this is from an ipeer
			ForwardMsgToOut(msg, bcastNW, bcastNWNo);
		}

		if( hasOptions && hasBorder && newMsg && (msgASNo != this->AS) ) {
			//if the Src IP of the msg is in my LAN Add to ePeer list else ignore
			bool present=true;
			//change present
			if(present) {
				list<Peer>::iterator it, IBEGIN=ePeers.begin(), IEND=ePeers.end();
				bool found=false;
				for( it=IBEGIN; it!=IEND; ++it ) 
					if( it->ASNo==msgASNo && it->uid==from ) {
						found=true; break;
					}
				if( !found ) {
					Peer p;
					p.uid = from;
					p.ASNo = msgASNo;
					p.ipAddress = senderIP;
					ePeers.push_back(p);
				}
			}
		}	

		/*Displaying the Topology Table */
		/*
		cout<<"\n TOPOLOGY TABLE STATE : \n";
		map<int, list<int> >::iterator mit, MBEGIN=this->topology.table.begin(), MEND=this->topology.table.end();
		for( mit=MBEGIN; mit!=MEND; ++mit ) {
			cout<<"\nRouter : "<<mit->first<<" Connected to : ";
			list<int>::iterator cns, CNBEGIN=mit->second.begin(), CNEND=mit->second.end();
			for( cns=CNBEGIN; cns!=CNEND; ++cns ) 
				cout<<"\t"<<(*cns);
		}
		*/
		/* Displaying and checking - START */
		/*
		cout<<"\nConnected Networks :\n";
		list<int>::iterator nw, NWBEGIN=nws.begin(), NWEND=nws.end();
		for( nw=NWBEGIN; nw!=NWEND; ++nw ) {
			cout<<"\tNW - "<<(*nw)<<endl;
		}
		cout<<"\n Injected Networks : \n";
		list<int>::iterator inw, INWBEGIN=injnws.begin(), INWEND=injnws.end();
		for( inw=INWBEGIN; inw!=INWEND; ++inw ) {
			cout<<"\tINW - "<<(*inw)<<endl;
		}
		list<Peer>::iterator it, IBEGIN=ePeers.begin(), IEND=ePeers.end();
		for( it=IBEGIN; it!=IEND; ++it ) {
			cout<<"\nE Peer Found \n";
			cout<<" ID : "<<it->uid;
			cout<<"\n AS : "<<it->ASNo;
		}
		IBEGIN=iPeers.begin(), IEND=iPeers.end();
		for( it=IBEGIN; it!=IEND; ++it ) {
			cout<<"\nI Peer Found \n";
			cout<<" ID : "<<it->uid;
			cout<<"\n AS : "<<it->ASNo;
		}
		*/
		/* Displaying and checking - END */

		s.clear();
		return 0;
	}
	

	/* Forward LSA */
	int ForwardMsgToOut( string msg, string fromNW, int fromNWNo ) {
		// Forward copies after modifying the msgs & fwd to only other lans
		int index=0;
		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) {
			if( i->netID != fromNWNo ) {
				//Modify the msg and send it
				string modMsg = msg;
				index = modMsg.find( fromNW + ",99", index );
				stringstream y;
				if( i->netID < 10 ) y << "0";
				y << i->netID;
				modMsg.replace(index, 2, y.str());
				ofstream outfile( outFileName.c_str(), std::ios::app);
				outfile<<modMsg<<endl;
				outfile.close();
			}
		}

		return 0;
	}

/*End of class*/
};

/*
 * Populate the HBGP data onto the global map files
 */
int PopulateHBGPData( ) {
	//map<int,int> ProviderMap;
	//map<int,int> PeerMap;
	stringstream ss;
	string hbgpFile = "HBGP.txt";
	ifstream inputfile( hbgpFile.c_str());
	string line, part;
	if( inputfile.is_open() ) {
		while(std::getline(inputfile,line)) {
			ss.str(line);
			int partIndex = 1;
			string relation;
			int LAS=0, RAS=0;
			while( std::getline(ss, part, ' ') ) {
				if( partIndex == 1 ) 
					LAS = toInt(part);
				else if( partIndex == 3 )
					RAS = toInt(part);
				else if( partIndex == 2 ) 
					relation = part;
				partIndex++;
			}
			if( relation == "-->" ) 
				ProviderMap[LAS] = RAS;
			else if ( relation == "-" ) 
				PeerMap[LAS] = RAS;
			
			ss.clear();
		}
	}
	inputfile.close();

	return 0;
}


/*
 * Main function
 */
int main( int argc, char ** argv ) {
	/* Processing the input arguments */
	string s(argv[1]);
	int uid = toInt(s);
	
	string s2(argv[2]);
	int asNo = toInt(s2);

	int nwcount = ( argc - 3 )/2; //Number of IPs, ignoring the program name and uid
	
	BRouter * ir = new BRouter( uid, asNo, nwcount );
	
	int nn = 0, hh = 0;
	/* Keep reading the IP addresses and add it to the routers name list */
	/* Streams are used to read the string as a 2 digit integer as per the specification */
	for( int i=3; i<argc-1; i+=2 ) {
		string sn(argv[i]);
		string sh(argv[i+1]);
		nn = toInt(sn);
		hh = toInt(sh);
		ID * ip = new ID( nn, hh);
		ir->names.push_back(*ip);
	}

	//Poplulate the HBGP Data
	PopulateHBGPData( );
	/* Calling populate peer provider list for this router */
	ir->PopulatePeerProviderList();
	/* Calling operations to be performed with the router lifetime parameter */
	ir->OperationsController(90);

	return 0;	
}
