/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Implementing OSPF and BGP
 * Sub module : controller
 */

#include<iostream>
#include<fstream>
#include<sstream>
#include<unistd.h> //For sleep
#include<list>
#include<map>

using namespace std;

/*
 * Controller Class - Acts as a medium of communication between the routers
 */
class Controller {

    public:
	list<int> networks; //List of active networks learnt by the controller
	map<int, long int> lastBcastID; //Map each router with its last seen broadcast id
	map<int, long int> lastRead; //Map to maintain the lastreadbyte of a OUTXX file. Mapping XX -> lastReadByte
	//vector< map<int, long int> >lastBcastIDToNW;

	/* Utility variables */
	stringstream s;
	int lastRouterFileRead;

	Controller() {
		this->lastRouterFileRead = 0;
		init();
	}

	/* Identfies the NETYY files that needs to be generated from OUTXX files */
	int init() {
		list<int>::iterator i, BEGIN = this->networks.begin(), END = this->networks.end();
		for( i=BEGIN; i!=END; ++i ) 
			lastRead[*i] = lastBcastID[*i] = 0;

		return 0;	
	}
	
	/* Routine to update the Router list - Called every second to identify if a new OUTXX file needs to be read */
	int UpdateRouterList( ) {
		ifstream inputfile("Routers");
		inputfile.seekg(lastRouterFileRead, inputfile.beg);
		stringstream ss;

		string line;
		while(std::getline(inputfile,line)) {
			//cout<<"\n\tLine : "<<line;
			int routerID = 0;
			ss.clear();
			ss.str(line);
			ss >> routerID;
			this->networks.push_back(routerID);
			lastRead[routerID] = lastBcastID[routerID] =  0;
			lastRouterFileRead = inputfile.tellg();
		}
		inputfile.close();

		return 0;
	}

	/*
	 * Operations controller routine calls various tasks that the controller need to perform at appropriate time intervals
	 */	
	int OperationsController( int lifetime ) {
		int i = lifetime;

		while( --i > 0 ) {
			/* Reading Net files associated with the router every second */
			ReadOutFiles(i);
			sleep(1);
		}

		return 0;
	}

	/*
	 * Reads the OUTXX files and appends the data to NETYY files
	 */
	int ReadOutFiles( int tick ) {
		UpdateRouterList(); //check if new Routers are added, if so add them to the networks list
		list<int>::iterator i, BEGIN = this->networks.begin(), END = this->networks.end();
		for( i=BEGIN; i!=END; ++i ) {
			s.str("");
			s << "OUT"; 
			if( (*i) < 10 ) s << "0";
			s << (*i);
			string filename = s.str();
			ifstream inputfile( filename.c_str());
			if( inputfile.is_open() ) {
				inputfile.seekg(lastRead[*i], inputfile.beg);
				/* From messages from last left state */
				string line;
				while(std::getline(inputfile,line)) {
					HandleMsg(line);
					lastRead[*i] = inputfile.tellg();
				}
			}
			inputfile.close();
		}

		//cout<<"\nReturning from ReadOutFiles ";
		return 0;
	}

	/* 
	 * Routine to handle a message from a sender
	 * Checks if the message is a new message and appends to the NETYY files if so
	 */
	int HandleMsg( string msg ) {
		//cout<<"\n Calling Handle Msg for : "<<msg;
		string part;
		s.str(msg);

		int partIndex = 1;
		bool newMsg = false;
		string yy;
		int sender = -1;

		while( std::getline(s, part, ' ') ) {
			if( partIndex == 2 ) {
				/* Identify which YY it should write the msg */
				yy = part.substr(1,2);
			}
			
			if( partIndex == 4 ) {
				//Find sender here
				stringstream ss(part);
				ss >> sender;
				ss.clear();
			}

			if( partIndex == 5 ) {
				/* This is the bcast number of this msg, check if this is a new msg, if so set the newMsg flag */
				stringstream ss(part);
				int curBcastID;
				ss >> curBcastID;
				//cout<<"\n Curr Broadcast ID = "<<curBcastID<<"  Last Bcast ID : "<<lastBcastID[sender];
				//cout<<"\nChecking last bcast id : "<<lastBcastID[sender]<<" sender : "<<sender<<"  curBcast ID "<<curBcastID;
				//Need to modify the below condition as to check the YY last sent by this sender
				//if( lastBcastID[sender] < curBcastID ) {
				//	lastBcastID[sender] = curBcastID;
					newMsg = true;
				//}
				ss.clear();
			}

			partIndex++;
		}

		if( newMsg ) {
			WriteNetFiles(msg, yy);
		}
		
		s.clear();
		return 0;
	}

	/*
	 * Given a message and identifies YY, this routine writes it to the NET'YY' file.
	 */
	int WriteNetFiles( string msg, string yy ) {
		stringstream ss;
		ss.str("");
		ss << "NET" << yy;
		string filename = ss.str();
		
		ofstream outfile( filename.c_str(), std::ios::app);
		
		if( outfile.is_open() ) {
			outfile.seekp(0, outfile.end);
			outfile<<msg;
			outfile<<endl;
		}
		
		outfile.close();
		ss.clear();
		return 0;
	}

};


int main() {
	Controller * ctrl = new Controller();
	ctrl->OperationsController(100);

	return 0;
}
