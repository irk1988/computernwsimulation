/*
 * Author: Immanuel Rajkumar Philip Karunakaran
 * Implementing OSPF and BGP
 * Sub module : irouter
 */
#include<iostream>
#include<sstream>
#include<fstream>
#include<unistd.h> /* For sleep */
#include<list>
#include<set>
#include<map>

#include "Graph.h"

using namespace std;

/*
 * Each router is associated with a netID and hostID. This structure defines a container for holding the identification details
 */
struct ID {
    public:
	int netID; /* NN -network ID - The network ID to which this router belongs to */
	int hostID; /* HH - Unique host name/ID for this router in a network */

	/* Constructors - 1) Parameterized 2) Default */
	ID (int n, int h ) {
		netID = n;
		hostID = h;
	}
};

/*
 * Data Structure to store the topology table for this router.
 * Contains mapping of router --> Lans mapping
 */
struct TopologyTable {
    public:
	map<int, list<int> > table;
};


/*
 * Routing table structure
 */
struct Path {
	int srcLan;
	int destLan;
	int next_hop;
	int dist;
};


/* Utility function to convert string to int */
int toInt( string s ){
	int result = 0;
	stringstream ss(s);
	ss >> result;
	ss.clear();
	return result;
}
	
/*
 * Router Type - irouter
 * Each router is identfied by (NN,HH)
 */
class IRouter {

    public:
	list<ID> names; /* Each router is associated with many network. This list contains the (NN,HH) address information of the router*/
	int uid;  /* Unique ID for a router in the whole network */
	int nwcount; /* Number of networks the router is connected to. Programatically, size of name list */
	int bcastID;

	string outFileName;
	string routingFileName;

	map<int, long int> lastRead; //Map to maintain the lastreadbyte of a netYY file. Mapping YY -> lastReadByte
	map<int, map<int, long int> > lastBCastIDforNW; // Map to maintain lastBcastIDforNW[uid][nwNo]	
	
	/* Utility variable for string and int manipulation */
	stringstream s;

	/* Routing table information */
	TopologyTable topology;
	list<Path> RoutingTable;

	IRouter(int uniqueID, int netCount) {
		uid = uniqueID;
		nwcount = netCount;
		bcastID = 1;

		//Setting the out file name in the constructor
		stringstream ss("");
		ss << "OUT";
		if( this->uid < 10 ) ss << "0";
		ss << this->uid;
		this->outFileName = ss.str();
		ss.clear();
		
		//Setting the routing file name in constructor
		stringstream yy;
		yy << "RT";
		if( this->uid < 10 ) yy << "0";
		yy << this->uid;
		this->routingFileName = yy.str();
		yy.clear();
	
    	}
	
	/*
	 * This routine calls the various functions of the router, at the appropriate time interval 
	 * Ex. LSA advertisements need to be done every 10 secs, this routine calls LSA advertisement routine every 10 secs 
         */
	int OperationsController( int lifetime ) {
		initLastReadState();
		initAliveMsg();
		int i = lifetime;

		while( --i > 0 ) {
			if( i % 10 == 0 ) //Need to change later
				LSAaddvertise(i);
			
			/* Reading Net files associated with the router every second */
			ReadNetFiles(i);

			/* Compute the routing table every 5 sec (Change the time later)*/ 
			if( i % 15 == 0 )
				ComputerRoutingTable(i);
				
			//DisplayRoutingTable(i);
			sleep(1);
		}
		return 0;
	}
	
	/* This routine computes the Routing table as per the schedule - Uses a graph class from external file */
	int ComputerRoutingTable( int tick ) {
		int src = this->uid;

		set<int> LANS; //using a set data structure to store the unique LANS // NOTE: In set there are no duplicates
		map<int, list<int> >::iterator mit, MBEGIN=this->topology.table.begin(), MEND=this->topology.table.end();
		for( mit=MBEGIN; mit!=MEND; ++mit ) {
			list<int>::iterator cns, CNBEGIN=mit->second.begin(), CNEND=mit->second.end();
			for( cns=CNBEGIN; cns!=CNEND; ++cns ) {
				LANS.insert(*cns);
			}	
		}
	
		int vn = LANS.size();
	
		Graph g(vn);

		for( mit=MBEGIN; mit!=MEND; ++mit ) {
			list<int>::iterator ii, CNBEGIN=mit->second.begin(), CNEND=mit->second.end();
			for( ii=CNBEGIN; ii!=CNEND; ++ii ) {
				list<int>::iterator jj;
				for( jj = ii; jj!=CNEND; ++jj ) {
					if( ii != jj )
						g.addEdge((*ii),(*jj),1,mit->first);
				}
			}	
		}
		
		driver(g, src, tick, routingFileName);	
		
		return 0;
	}

	/* Put an entry into 'Routers' table stating it is alive now. This is used by controller so that it can read OUTXX files */
	int initAliveMsg() {
		ofstream outfile( "Routers", std::ios::app);
		stringstream ss;
		if ( this->uid < 10 ) ss << "0";
		ss << this->uid;
		outfile<<ss.str()<<endl;
		outfile.close();
		
		return 0;
	}

	/* Routine to read the NETYY files and update the routing table of the irouter */
	int ReadNetFiles( int tick ) {
		
		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) {
			s.str("");
			s << "NET";
			if ( i->netID < 10 ) s <<"0";
			s << i->netID;
			string filename = s.str();
			
			ifstream inputfile( filename.c_str());
			if( inputfile.is_open() ) {
				inputfile.seekg(lastRead[i->netID], inputfile.beg);
				string line;
				while(std::getline(inputfile,line)) {
					HandleNetFilesLine( line );
					lastRead[i->netID] = inputfile.tellg();
				}
			}
			inputfile.close();
			s.clear();
		}

		return 0;
	}


	/* Handles the net files line read. Appends the line to its own OUTXX file if its not its own broadcase */
	int HandleNetFilesLine( string msg ) {
		string part;
		s.str(msg);
		
		int partIndex = 1;
		int from = 0;
		bool msgFromMe = true, newMsg = false, nwStart=false;
		string bcastNW;
		int bcastNWNo;
		list<int> nws;

		while( std::getline(s, part, ' ') ) {
			if( partIndex == 2 ) {
				bcastNW = part.substr(1,2);
				bcastNWNo = toInt(bcastNW);
			}

			if( partIndex == 4 ) {
				//Get the router ID from this part, check its bcast id
				from = toInt(part);
				//If this msg is not from me, then handle it.
				if( from != this->uid ) {
					msgFromMe = false;
				}
			}
		
			if( partIndex == 5 ) {
				//check the broadcast id if this is a msg from a different router
				if( !msgFromMe ) {
					int curBcastID = toInt(part);
					//Are we reading a msg from this reouter for the first time, check if there is n entry in the map
					if( lastBCastIDforNW[from][bcastNWNo] < curBcastID ) {
						lastBCastIDforNW[from][bcastNWNo] = curBcastID;
						newMsg = true;
					}
				}
			}
			
			if( part == "NETWKS" ) 
				nwStart = true;
		
			if( part == "OPTIONS" ) 
				nwStart = false;
			
			if( nwStart ) {
				int conNW = 0;
				conNW = toInt(part);
				nws.push_back(conNW);
			}
	
			partIndex++;
		}
		
		if( nws.size() > 1 ) 
			nws.erase(nws.begin());
		
		if( newMsg ) {
			//Write the new msg to the OUTXX file - but fwd to only to others - reverse poison
			int index=0;
			
			list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
			for( i=BEGIN; i!=END; ++i ) {
				if( i->netID != bcastNWNo ) {
					//Modify the msg and send it
					string modMsg = msg;
					index = modMsg.find( bcastNW + ",99", index );
					stringstream y;
					if( i->netID < 10 ) y << "0";
					y << i->netID;
					modMsg.replace(index, 2, y.str());
					ForwardMsgToOut( modMsg );
				}
			}
			//Update the routing table
			this->topology.table[from] = nws;	
		}
		s.clear();
		return 0;
	}

	/* Forward LSA */
	int ForwardMsgToOut( string msg ) {
		// Forward copies after modifying the msgs & fwd to only other lans
		ofstream outfile( outFileName.c_str(), std::ios::app);
		outfile<<msg<<endl;
		outfile.close();
		return 0;
	}
	
	/* Routine to create an LSA message and post it in the OUTXX file of the irouter */
	int LSAaddvertise( int tick ) {
		ofstream outfile( outFileName.c_str(), std::ios::app);

		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		for( i=BEGIN; i!=END; ++i ) {
			outfile<<"(";
			if( i->netID < 10 ) outfile<<"0";
			outfile<<i->netID<<",";
			if( i->hostID < 10 ) outfile<<"0";
			outfile<<i->hostID<<") (";
			if( i->netID < 10 ) outfile<<"0";
			outfile<<i->netID<<",99) LSA ";
			if( this->uid < 10 ) outfile<<"0";
			outfile<<this->uid<<" "<<bcastID<<" NETWKS ";
			for( k=BEGIN; k!=END; ++k ) {
				if( k->netID < 10 ) outfile<<"0";
				outfile<<k->netID<<" ";
			}
			outfile<<endl;//flushing output to file
		}
		this->bcastID++;
		outfile.close();
		s.clear();
		return 0;
	}


	int initLastReadState() {
		list<ID>::iterator i , k, BEGIN = this->names.begin() , END = this->names.end();
		list<int> nws;
		for( i=BEGIN; i!=END; ++i ) { 
			lastRead[i->netID] = 0;
			nws.push_back(i->netID);
		}
		this->topology.table[this->uid] = nws;	
		
		return 0;
	}
};


int main( int argc, char ** argv ) {
	/* Processing the input arguments */
	string s(argv[1]);
	int uid = toInt(argv[1]); // Get UID as int 
	
	int nwcount = ( argc - 2 )/2; //Number of IPs, ignoring the program name and uid
	
	IRouter * ir = new IRouter( uid, nwcount );
	
	int nn = 0, hh = 0;
	/* Keep reading the IP addresses and add it to the routers name list */
	/* Streams are used to read the string as a 2 digit integer as per the specification */
	for( int i=2; i<argc-1; i+=2 ) {
		string sn(argv[i]);
		string sh(argv[i+1]);
		nn=toInt(sn);
		hh=toInt(sh);
		ID * ip = new ID( nn, hh);
		ir->names.push_back(*ip);
	}

	/* Calling operations to be performed with the router lifetime parameter */
	ir->OperationsController(90);

	return 0;	
}
